import "./styles.scss";
import React from "react";
import * as Icon from "react-feather";
const BG = "#719ea6";
const BGS = "#f7d114";
const audioLinks = {
  Q: "https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3",
  W: "https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3",
  E: "https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3",
  A: "https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3",
  S: "https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3",
  D: "https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3",
  Z: "https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3",
  X: "https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3",
  C: "https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3"
};

const audioDisplay = {
  Q: "Heater 1",
  W: "Heater 2",
  E: "Heater 3",
  A: "Heater 4",
  S: "Clap",
  D: "Open HH",
  Z: "Kick n' Hat",
  X: "Kick",
  C: "Closed HH"
};
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      power: true,
      volume: 20,
      muted: false,
      keys: Object.keys(audioDisplay)
    };
    this.handlePower = this.handlePower.bind(this);
    this.handleKey = this.handleKey.bind(this);
    this.handleDisplay = this.handleDisplay.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleVolume = this.handleVolume.bind(this);
    this.handleMute = this.handleMute.bind(this);
  }

  handleMute() {
    this.setState({ muted: this.state.muted ? false : true });
  }

  handleDisplay(text) {
    document.getElementById("display").innerText = text;
  }

  handlePower(control) {
    this.setState({ power: control });
    let t = this.state.power ? "OFF" : "ON";
    this.handleDisplay("Power " + t);
  }
  handleKey(keyClick) {
    if (this.state.power) {
      let ele;
      ele = document.getElementById(keyClick);
      ele.volume = this.state.volume / 100;
      ele.play();
      ele = document.getElementById(keyClick + "-parent");
      ele.style.backgroundColor = BGS;
      setTimeout(() => {
        ele.style.backgroundColor = BG;
      }, 500);
      this.handleDisplay(audioDisplay[keyClick]);
    }
  }
  handleKeyPress(event) {
    let key = event.key.toUpperCase();
    if (window.innerWidth > 750) {
      if (Object.keys(audioDisplay).includes(key)) {
        this.handleKey(key);
      }
    }
  }
  handleVolume(event) {
    this.setState({ volume: event.target.value, muted: false });
    this.handleDisplay("Volume : " + this.state.volume);
  }
  componentDidMount() {
    window.addEventListener("keypress", this.handleKeyPress);
  }
  render() {
    return (
      <div className="App">
        <div className="drum">
          <div className="title">
            <h1>e-Drum</h1>
          </div>
          <div className="keys">
            {this.state.keys.map((keyElement, index) => (
              <div
                className="key"
                key={index}
                onClick={() => this.handleKey(keyElement)}
                id={keyElement + "-parent"}
              >
                <span>
                  {keyElement}
                  <audio id={keyElement} muted={this.state.muted}>
                    <source src={audioLinks[keyElement]}></source>
                  </audio>
                </span>
              </div>
            ))}
          </div>
          <div className="controls">
            <div className="power">
              <span className="power-title">Power</span>
              <div className="power-control">
                <span
                  style={{
                    backgroundColor: this.state.power ? "green" : "black"
                  }}
                  onClick={() => this.handlePower(true)}
                ></span>
                <span
                  style={{
                    backgroundColor: this.state.power
                      ? "black"
                      : "rgb(168, 52, 34)"
                  }}
                  onClick={() => this.handlePower(false)}
                ></span>
              </div>
            </div>
            <div className="display">
              <span id="display"></span>
            </div>
            <div className="volume">
              {this.state.muted ? (
                <Icon.VolumeX onClick={this.handleMute} />
              ) : (
                <Icon.Volume2 onClick={this.handleMute} />
              )}
              <input
                type="range"
                min="0"
                max="100"
                id="volume"
                value={this.state.volume}
                onChange={this.handleVolume}
              />
            </div>
          </div>
        </div>
        <div className="byme">
          <h3>
            by : <a href="https://suhascv.netlify.app">suhascv</a>
          </h3>
        </div>
      </div>
    );
  }
}
